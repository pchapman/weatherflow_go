package weatherflow_go

type DeviceMeta struct {
	Agl float64 `json:"agl"`
	Name string `json:"name"`
	Environment DeviceEnv `json:"environment"`
	WifiNetworkName string `json:"wifi_network_name"`
}

type DeviceSettings struct {
	ShowPrecipFinal bool `json:"show_precip_final"`
}

type Device struct {
	Id int `json:"device_id"`
	SerialNumber string `json:"serial_number"`
	LocationId int `json:"location_id"`
	Meta DeviceMeta `json:"device_meta"`
	DeviceType string `json:"device_type`
	HardwareRevision string `json:"hardware_revision"`
	FirmwareRevision string `json:"firmware_revision"`
}

type StationItem struct {
	Id int `json:"location_item_id"`
	LocationId int `json:"location_id"`
	DeviceId int `json:"device_id"`
	Item string `json:"item"`
	Sort int `json:"sort"`
	StationId int `json:"station_id"`
	StationItemId int `json:"station_item_id"`
}

type StationMeta struct {
	ShareWithWF bool `json:"share_with_wf"`
	ShareWithWU bool `json:"share_with_wu"`
	Elevation float64 `json:"elevation"`
}

type StationUnits struct {
	Direction UnitsDirection `json:"units_direction"`
	Distance UnitsDistance `json:"units_distance"`
	Other     UnitsSystem `json:"units_other"`
	Precipitation UnitsPrecipitation `json:"units_precip"`
	Pressure UnitsPressure `json:"units_pressure"`
	Temp UnitsTemp `json:"units_temp"`
	WindSpeed UnitsSpeed `json:"units_wind"`
}

type Status struct {
	Code int `json:"status_code"`
	Message string `json:"status_message"`
}

type Station struct {
	Id int `json:"station_id"`
	StationName string `json:"station_name"`
	PublicName string `json:"public_name"`
	Latitude float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	TimeZone string `json:"timezone"`
	TimeZoneOffset int `json:"timezone_offset_minutes"`
	Meta StationMeta `json:"station_meta"`
	LastModifiedEpoch int64 `json:"last_modified_epoch"`
	CreatedEpoch int64 `json:"created_epoch"`
	Devices []Device `json:"devices"`
	IsLocalMode bool `json:"is_local_mode"`


	Status Status `json:"status"`
}

type Stations struct {
	Stations []Station
	Status Status
}
