package weatherflow_go

type Condition struct {
	Time                            int64             `json:"time"`
	Conditions                      string            `json:"conditions"`
	Icon                            string            `json:"icon"`
	AirTemperature                  float32           `json:"air_temperature"`
	SeaLevelPressure                float32           `json:"sea_level_pressure"`
	StationPressure                 float32           `json:"station_pressure"`
	PressureTrend                   PressureTrend     `json:"pressure_trend"`
	RelativeHumidity                float32           `json:"relative_humidity"`
	WindAvg                         float32           `json:"wind_avg"`
	WindDirection                   int               `json:"wind_direction"`
	WindDirectionCardinal           CardinalDirection `json:"wind_direction_cardinal"`
	WindGust                        float32           `json:"wind_gust"`
	SolarRadiation                  float32           `json:"solar_radiation"`
	UV                              float32           `json:"uv"`
	Brightness                      float32           `json:"brightness"`
	FeelsLike                       float32           `json:"feels_like"`
	DewPoint                        float32           `json:"dew_point"`
	WebBulbTemp                     float32           `json:"wet_bulb_temperature"`
	DeltaT                          float32           `json:"delta_t"`
	AirDensity                      float32           `json:"air_density"`
	LightningStrikeCountLast1Hr     int               `json:"lightning_strike_count_last_1hr"`
	LightningStrikeCountLast3Hr     int               `json:"lightning_strike_count_last_3hr"`
	PrecipAccumLocalDay             float32           `json:"precip_accum_local_day"`
	PrecipAccumLocalYesterday       float32           `json:"precip_accum_local_yesterday"`
	PrecipMinutesLocalDay           int               `json:"precip_minutes_local_day"`
	PrecipMinutesLocalYesterday     int               `json:"precip_minutes_local_yesterday"`
	IsPrecipLocalDayRainCheck       bool              `json:"is_precip_local_day_rain_check"`
	IsPrecipLocalYesterdayRainCheck bool              `json:"is_precip_local_yesterday_rain_check"`
}

type DailyForecast struct {
	DayStartLocal      int64             `json:"day_start_local"`
	DayNum             int               `json:"day_num"`
	MonthNum           int               `json:"month_num"`
	Conditions         string            `json:"conditions"`
	Icon               string            `json:"icon"`
	Sunrise            int64             `json:"sunrise"`
	Sunset             int64             `json:"sunset"`
	AirTempHigh        float32           `json:"air_temp_high"`
	AirTempLow         float32           `json:"air_temp_low"`
	PrecipProbablility int8              `json:"precip_probablility"`
	PrecipIcon         string            `json:"precip_icon"`
	PrecipType         PrecipitationType `json:"precip_type"`
}

type HourlyForecast struct {
	Time                  int64             `json:"time"`
	Conditions            string            `json:"conditions"`
	Icon                  string            `json:"icon"`
	AirTemp               float32           `json:"air_temperature"`
	SeaLevelPressure      float32           `json:"sea_level_pressure"`
	RelativeHumidity      int8              `json:"relative_humidity"`
	PrecipProbablility    int8              `json:"precip_probablility"`
	PrecipIcon            string            `json:"precip_icon"`
	WindAvg               float32           `json:"wind_avg"`
	WindDirection         int               `json:"wind_direction"`
	WindDirectionCardinal CardinalDirection `json:"wind_direction_cardinal"`
	WindGust              float32           `json:"wind_gust"`
	UV                    float32           `json:"uv"`
	FeelsLike             float32           `json:"feels_like"`
	LocalHour             int8              `json:"local_hour"`
	LocalDay              int8              `json"local_day"`
}

type Forecast struct {
	Daily  []DailyForecast  `json:"daily"`
	Hourly []HourlyForecast `json:"hourly"`
}

type ForecastUnits struct {
	Temp            UnitsTemp           `json:"units_temp"`
	WindSpeed       UnitsSpeed          `json:"units_wind"`
	Precipitation   UnitsPrecipitation  `json:"units_precip"`
	Pressure        UnitsPressure       `json:"units_pressure"`
	Distance        UnitsDistance       `json:"units_distance"`
	Brightness      UnitsBrightness     `json:"units_brightness"`
	SolarRadiation  UnitsSolarRadiation `json:"units_solar_radiation"`
	Other           UnitsSystem         `json:"units_other"`
	UnitsAirDensity UnitsAirDensity     `json:"units_air_density"`
}

type StationForecast struct {
	Latitude         float32       `json:"latitude"`
	Longitude        float32       `json:"longitude"`
	Timezone         string        `json:"timezone"`
	TimeZoneOffset   int           `json:"timezone_offset_minutes"`
	LocationName     string        `json:"location_name"`
	CurrentCondition Condition     `json:"current_condition"`
	Forecast         Forecast      `json:"forecast"`
	Status           Status        `json:"status"`
	Units            ForecastUnits `json:"units"`
}
