package weatherflow_go

type CardinalDirection string
const (
	DirectionN CardinalDirection = "N"
	DirectionNNE CardinalDirection = "NNE"
	DirectionNE CardinalDirection = "NE"
	DirectionENE CardinalDirection = "ENE"
	DirectionE CardinalDirection = "E"
	DirectionESE CardinalDirection = "ESE"
	DirectionSE CardinalDirection = "SE"
	DirectionSSE CardinalDirection = "SSE"
	DirectionS CardinalDirection = "S"
	DirectionSSW CardinalDirection = "SSW"
	DirectionSW CardinalDirection = "SW"
	DirectionWSW CardinalDirection = "WSW"
	DirectionW CardinalDirection = "W"
	DirectionWNW CardinalDirection = "WNW"
	DirectionNW CardinalDirection = "NW"
	DirectionNNW CardinalDirection = "NNW"
)

type DeviceEnv string
const (
	DeviceEnvIndoor DeviceEnv = "indoor"
	DeviceEnvOutdoor DeviceEnv = "outdoor"
)

type PrecipitationType string
const (
	PrecipitationRain PrecipitationType = "rain"
	PrecipitationSnow PrecipitationType = "snow"
	PrecipitationSleet PrecipitationType = "sleet"
	PrecipitationStorm PrecipitationType = "storm"
)

type PressureTrend string
const (
	PressureTrendFalling = "falling"
	PressureTrendRising = "rising"
	PressureTrendSteady = "steady"
	PressureTrendUnknown = "unknown"
)

type UnitsAirDensity string

type UnitsBrightness string

type UnitsDirection string
const (
	UnitsDirectionCardinal UnitsDirection = "cardinal"
	UnitsDirectionDegrees UnitsDirection = "degrees"
)

type UnitsDistance string
const (
	UnitsDistanceMi UnitsDistance = "mi"
	UnitsDistanceKM UnitsDistance = "km"
)

type UnitsSystem string
const (
	UnitsOtherImperial UnitsSystem = "imperial"
	UnitsOtherMetric   UnitsSystem = "metric"
)

type UnitsPrecipitation string
const (
	UnitsPrecipitationIn UnitsPrecipitation = "in"
	UnitsPrecipitationMM UnitsPrecipitation = "mm"
	UnitsPrecipitationCM UnitsPrecipitation = "cm"
)

type UnitsPressure string
const (
	UnitsPressureInHg UnitsPressure = "inhg"
	UnitsPressureMB UnitsPressure = "mb"
	UnitsPressureMMHg UnitsPressure = "mmhg"
	UnitsPressureHPa UnitsPressure = "hpa"
)

type UnitsSolarRadiation string

type UnitsSpeed string
const (
	UnitsSpeedMPH UnitsSpeed = "mph"
	UnitsSpeedKPH UnitsSpeed = "kph"
	UnitsSpeedKTS UnitsSpeed = "kts"
	UnitsSpeedMPS UnitsSpeed = "mps"
	UnitsSpeedBTF UnitsSpeed = "btf"
	UnitsSpeedLFM UnitsSpeed = "lfm"
)

type UnitsTemp string
const (
	UnitsTempC UnitsTemp = "c"
	UnitsTempF UnitsTemp = "f"
)
