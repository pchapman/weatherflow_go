package weatherflow_go

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"testing"
)

func TestSendHelloWorld(t *testing.T) {
	key := os.Getenv("WF_KEY")
	if key == "" {
		t.Fatal("WF_KEY environment variable required")
	}
	stationStr := os.Getenv("WF_STATION_ID")
	if key == "" {
		t.Fatal("WF_STATION_ID environment variable required")
	}
	station, err := strconv.ParseInt(stationStr, 10, 64)
	if err != nil {
		t.Fatalf("WF_STATION_ID environment variable must be an integer value: %s", err)
	}

	stations, err := QueryStations(key)
	if err != nil {
		t.Fatalf("Unable to query for all stations: %s", err)
	}
	data, err := json.Marshal(stations)
	if err != nil {
		t.Fatalf("Unable to write stations json: %s", err)
	}
	fmt.Printf("\nStations:\n%s\n", data)

	obs, err := QueryLastestStationObservation(key, int(station))
	if err != nil {
		t.Fatalf("Unable to query for latest observation: %s", err)
	}
	data, err = json.Marshal(obs)
	if err != nil {
		t.Fatalf("Unable to write last observation to json: %s", err)
	}
	fmt.Printf("\nLatest Observation:\n%s\n", data)

	forecast, err := QueryLatestStationForecast(key, int(station))
	if err != nil {
		t.Fatalf("Unable to query for latest forecast: %s", err)
	}
	data, err = json.Marshal(forecast)
	if err != nil {
		t.Fatalf("Unable to write last forecast to json: %s", err)
	}
	fmt.Printf("\nLatest Forecast:\n%s\n", data)
}
