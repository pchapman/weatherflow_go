package weatherflow_go

type Observation struct {
	Timestamp int64 `json:"timestamp"`
	AirTemperature float32 `json:"air_temperature"`
	BarometricPressure float32 `json:"barometric_pressure"`
	StationPressure float32 `json:"station_pressure"`
	SeaLevelPressure float32 `json:"sea_level_pressure"`
	RelativeHumidity float32 `json:"relative_humidity"`
	Precip float32 `json:"precip"`
	PrecipAccumLastHour float32 `json:"precip_accum_last_1hr"`
	PrecipAccumLocalDay float32 `json:"precip_accum_local_day"`
	PrecipAccumLocalYesterday float32 `json:"precip_accum_local_yesterday"`
	PrecipMinutesLocalDay int `json:"precip_minutes_local_day"`
	PrecipMinutesLocalYesterday int `json:"precip_minutes_local_yesterday"`
	WindAverage float32 `json:"wind_avg"`
	WindDirection int `json:"wind_direction"`
	WindGust float32 `json:"wind_gust"`
	WindLull float32 `json:"wind_lull"`
	SolarRadiation int `json:"solar_radiation"`
	UV float32 `json:"uv"`
	Brightness int `json:"brightness"`
	LightningStrikeCount int `json:"lightning_strike_count"`
	LightningStrikeCountLast1Hr int `json:"lightning_strike_count_last_1hr"`
	LightningStrikeCountLast3Hr int `json:"lightning_strike_count_last_3hr"`
	FeelsLike float32 `json:"feels_like"`
	HeatIndex float32 `json:"heat_index"`
	WindChill float32 `json:"wind_chill"`
	DewPoint float32 `json:"dew_point"`
	WetBulbTemp float32 `json:"wet_bulb_temperature"`
	DeltaT float32 `json:"delta_t"`
	AirDensity float32 `json:"air_density"`
	PressureTrend PressureTrend `json:"pressure_trend"`
}

type Observations struct {
	StationId int `json:"station_id"`
	StationName string `json:"station_name"`
	PublicName string `json:"public_name"`
	Latitude float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	TimeZone string `json:"timezone"`
	Elevation float32 `json:"elevation"`
	IsPublic bool `json:"is_public"`
	Status Status `json:"status"`
	Units StationUnits `json:"station_units"`
	OutdoorKeys []string `json:"outdoor_keys"`
	List []Observation `json:"obs"`
}
