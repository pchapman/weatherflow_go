package weatherflow_go

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

func QueryStations(key string) (*Stations, error) {
	resp := new(Stations)
	url, err := url.Parse(fmt.Sprintf("https://swd.weatherflow.com/swd/rest/stations?token=%s", key))
	if err != nil {
		return resp, err
	}
	err = executeRestCall("GET", url, nil, resp)
	return resp, err
}

func QueryLatestStationForecast(key string, stationId int) (*StationForecast, error) {
	resp := new(StationForecast)
	url, err := url.Parse(fmt.Sprintf("https://swd.weatherflow.com/swd/rest/better_forecast?station_id=%d&token=%s", stationId, key))
	if err != nil {
		return resp, err
	}
	err = executeRestCall("GET", url, nil, resp)
	return resp, err
}

func QueryLastestStationObservation(key string, stationId int) (*Observations, error) {
	resp := new(Observations)
	url, err := url.Parse(fmt.Sprintf("https://swd.weatherflow.com/swd/rest/observations/station/%d?token=%s", stationId, key))
	if err != nil {
		return resp, err
	}
	err = executeRestCall("GET", url, nil, resp)
	return resp, err
}

func executeRestCall(action string, u *url.URL, data []byte, results interface{}) error {
	client := &http.Client{}

	log.Printf("WFGO: Executing %s on endpoint %s with data %s", action, u, data)

	req, err := http.NewRequest(action, u.String(), bytes.NewBuffer(data))
	if err != nil {
		log.Printf("WFGO: Error building http request for %s against %s: %s\n", action, u, err)
		return err
	}
	req.Header.Add("accept", "application/json")
	req.Header.Add("content-type", "application/json")

	log.Printf("WFGO: Executing %s against endpoint %s", action, u)
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("WFGO: Got error for %s against %s: %s\n", action, u, err)
		return err
	}
	log.Printf("WFGO: Got resonse code %s", resp.Status)
	defer resp.Body.Close()

	log.Printf("WFGO: Response with status code %d for %s against endpoint %s", resp.StatusCode, action, u)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("WFGO: Got error getting response body for %s against %s: %s\n", action, u, err)
		return err
	}
	if resp.StatusCode != 200 {
		if resp.StatusCode == 204 && results == nil {
			// This is fine.  Processed ok, no content, but we don't expect any.
		} else {
			s := string(body)
			log.Printf("WFGO: Got status code %d for %s against %s: %s\n", resp.StatusCode, action, u, s)
			return errors.New(s)
		}
	}
	log.Printf("WFGO: Response with body %s for %s against endpoint %s", string(body), action, u)
	if results != nil {
		err = json.Unmarshal(body, results)
	}
	return err
}
